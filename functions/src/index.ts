import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const firebaseHelper = require('firebase-functions-helper');
import * as express from 'express';
import * as bodyParser from "body-parser";

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();
const app = express();
const main = express();
const contactsCollection = 'contacts';
const bus_ = 'bus';
const ticket_ = 'ticket';
const ticketExpired_ = 'ticketExpired';
const purchase_ = 'purchase';
const facilities_ = 'facilities';
const class_ = 'class';
const user_ = 'user';
const seatBooking_ = 'seatBooking';
const location_ = 'location';
const chatting_ = 'chatting';
const banner_ = 'banner';
const brands_ = 'brands';

main.use('/api/v1', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));
// webApi is your functions name, and you will pass main as 
// a parameter
export const webApi = functions.https.onRequest(main);

// Add new contact
app.post('/contacts', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, contactsCollection, req.body);
    res.send('Create a new contact');
})
// Update new contact
app.patch('/contacts/:contactId', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, contactsCollection, req.params.contactId, req.body);
    res.send('Update a new contact');
})
// View a contact

app.get('/contacts/:contactId', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, contactsCollection, req.params.contactId)
        .then(doc => res.status(200).send(doc));
        //res.send('200');	
})

// View all contacts
app.get('/contacts', (req, res) => {
    firebaseHelper.firestore
        .backup(db, contactsCollection)
        .then(data => res.status(200).send(data))
})


// Delete a contact 
app.delete('/contacts/:contactId', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, contactsCollection, req.params.contactId);
    res.send('Document deleted');
})

//-----------------------------------
app.post('/bus', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, bus_, req.body);
    res.send('Create a new contact');
})
// Update new bus
app.patch('/bus/:platId', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, bus_, req.params.platId, req.body);
    res.send('Update a new contact');
})
// View a bus

app.get('/bus/:platId', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, bus_, req.params.platId)
        .then(doc => res.status(200).send(doc));
        //res.send('200');	
})

// View all bus
app.get('/bus', (req, res) => {
    firebaseHelper.firestore
        .backup(db, bus_)
        .then(data => res.status(200).send(data))
})


// Delete a bus
app.delete('/bus/:platId', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, bus_, req.params.platId);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/location', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, location_, req.body);
    res.send('Create a new location');
})
// Update new location
app.patch('/location/:name', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, location_, req.params.name, req.body);
    res.send('Update a new location');
})
// View a location

app.get('/location/:name', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, location_, req.params.name)
        .then(doc => res.status(200).send(doc));
        //res.send('200');	
})

// View all location
app.get('/location', (req, res) => {
    firebaseHelper.firestore
        .backup(db, location_)
        .then(data => res.status(200).send(data))
})


// Delete a location 
app.delete('/location/:name', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, location_, req.params.name);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/ticket', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, ticket_, req.body);
    res.send('Create a new ticket');
})
// Update new ticket
app.patch('/ticket/:noTicket', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, ticket_, req.params.noTicket, req.body);
    res.send('Update a new ticket');
})
// View a ticket

app.get('/ticket/:noTicket', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, ticket_, req.params.noTicket)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all ticket
app.get('/ticket', (req, res) => {
    firebaseHelper.firestore
        .backup(db, ticket_)
        .then(data => res.status(200).send(data))
})


// Delete a ticket 
app.delete('/ticket/:noTicket', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, ticket_, req.params.noTicket);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/purchase', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, purchase_, req.body);
    res.send('Create a new purchase');
})
// Update new purchase
app.patch('/purchase/:noPurchase', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, purchase_, req.params.noPurchase, req.body);
    res.send('Update a new purchase');
})
// View a purchase

app.get('/purchase/:noPurchase', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, purchase_, req.params.noPurchase)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all purchase
app.get('/purchase', (req, res) => {
    firebaseHelper.firestore
        .backup(db, purchase_)
        .then(data => res.status(200).send(data))
})


// Delete a purchase 
app.delete('/purchase/:noPurchase', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, purchase_, req.params.noPurchase);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/facilities', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, facilities_, req.body);
    res.send('Create a new facilities');
})
// Update new facilities
app.patch('/facilities/:name', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, facilities_, req.params.name, req.body);
    res.send('Update a new facilities');
})
// View a facilities

app.get('/facilities/:name', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, facilities_, req.params.name)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all facilities
app.get('/facilities', (req, res) => {
    firebaseHelper.firestore
        .backup(db, facilities_)
        .then(data => res.status(200).send(data))
})


// Delete a facilities 
app.delete('/facilities/:name', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, facilities_, req.params.name);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/class', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, class_, req.body);
    res.send('Create a new class');
})
// Update new class
app.patch('/class/:name', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, class_, req.params.name, req.body);
    res.send('Update a new class');
})
// View a class

app.get('/class/:name', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, class_, req.params.name)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all class
app.get('/class', (req, res) => {
    firebaseHelper.firestore
        .backup(db, class_)
        .then(data => res.status(200).send(data))
})


// Delete a class 
app.delete('/class/:name', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, class_, req.params.name);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/chatting/:email', (req, res) => {
/*firebaseHelper.firestore
        .createNewDocument(db, chatting_, req.body);
        res.send('Create a new chatting');*/
    let emailParam = req.params.email;
    let bodyParam = req.body;
    db.collection(chatting_).doc(emailParam).collection("chatdata").add(bodyParam)
        .then(function (docRef) {
            res.send(200).json({
                message: 'Chatting created'
            });
        })
        .catch(function (error) {
            res.send(400).json({
                message: error
            });
        });
})

// Update new chatting
app.patch('/chatting/:name', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, chatting_, req.params.name, req.body);
    res.send('Update a new chatting');
})

// View a chatting
app.get('/chatting/:email', (req, res) => {
    firebaseHelper.firestore
        .backup(db, chatting_, "chatdata")
        .then(data => {
            let paramEmail = req.params.email;
            let docs = data[chatting_];
            for (const key in docs) {
                if (key == paramEmail) {
                    let docUser = docs[paramEmail];
                    res.status(200).send(docUser);
                    break;
                }
            }
            res.status(400).json({
                message: "Data not found"
            });
        })
})

// View all chatting
app.get('/chatting', (req, res) => {
    firebaseHelper.firestore
        .backup(db, chatting_, "chatdata")
        .then(data => res.status(200).send(data))
})


// Delete all chatting by email
app.delete('/chatting/:email', (req, res) => {
/*firebaseHelper.firestore
        .deleteDocument(db, chatting_, req.params.email);
        res.send('Document deleted');*/
        res.status(200).json({
            message: "API delete chatting coming soon"
        });
})


//-----------------------------------
app.post('/brands', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, brands_, req.body);
    res.send('Create a new brands');
})
// Update new brands
app.patch('/brands/:name', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, brands_, req.params.name, req.body);
    res.send('Update a new brands');
})
// View a brands

app.get('/brands/:name', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, brands_, req.params.name)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all brands
app.get('/brands', (req, res) => {
    firebaseHelper.firestore
        .backup(db, brands_)
        .then(data => res.status(200).send(data))
})


// Delete a brands 
app.delete('/brands/:name', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, brands_, req.params.name);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/banner', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, banner_, req.body);
    res.send('Create a new banner');
})
// Update new banner
app.patch('/banner/:url', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, banner_, req.params.url, req.body);
    res.send('Update a new banner');
})
// View a banner

app.get('/banner/:url', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, banner_, req.params.url)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all banner
app.get('/banner', (req, res) => {
    firebaseHelper.firestore
        .backup(db, banner_)
        .then(data => res.status(200).send(data))
})


// Delete a banner 
app.delete('/banner/:url', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, banner_, req.params.url);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/seatBooking', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, seatBooking_, req.body);
    res.send('Create a new seatBooking');
})
// Update new seatBooking
app.patch('/seatBooking/:seatNumbers', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, seatBooking_, req.params.seatNumbers, req.body);
    res.send('Update a new seatBooking');
})
// View a seatBooking

app.get('/seatBooking/:seatNumbers', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, seatBooking_, req.params.seatNumbers)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all seatBooking
app.get('/seatBooking', (req, res) => {
    firebaseHelper.firestore
        .backup(db, seatBooking_)
        .then(data => res.status(200).send(data))
})


// Delete a seatBooking 
app.delete('/seatBooking/:seatNumbers', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, seatBooking_, req.params.seatNumbers);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/ticketExpired', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, ticketExpired_, req.body);
    res.send('Create a new ticketExpired');
})
// Update new ticketExpired
app.patch('/ticketExpired/:noTicket', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, ticketExpired_, req.params.noTicket, req.body);
    res.send('Update a new ticketExpired');
})
// View a ticketExpired

app.get('/ticketExpired/:noTicket', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, ticketExpired_, req.params.noTicket)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all ticketExpired
app.get('/ticketExpired', (req, res) => {
    firebaseHelper.firestore
        .backup(db, ticketExpired_)
        .then(data => res.status(200).send(data))
})


// Delete a ticketExpired 
app.delete('/ticketExpired/:noTicket', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, ticketExpired_, req.params.noTicket);
    res.send('Document deleted');
})


//-----------------------------------
app.post('/user', (req, res) => {
    firebaseHelper.firestore
        .createNewDocument(db, user_, req.body);
    res.send('Create a new user');
})
// Update new user
app.patch('/user/:username', (req, res) => {
    firebaseHelper.firestore
        .updateDocument(db, user_, req.params.username, req.body);
    res.send('Update a new user');
})
// View a user

app.get('/user/:username', (req, res) => {
    firebaseHelper.firestore
        .getDocument(db, user_, req.params.username)
        .then(doc => res.status(200).send(doc));
        //res.send('200');  
})

// View all user
app.get('/user', (req, res) => {
    firebaseHelper.firestore
        .backup(db, user_)
        .then(data => res.status(200).send(data))
})


// Delete a user 
app.delete('/user/:username', (req, res) => {
    firebaseHelper.firestore
        .deleteDocument(db, user_, req.params.username);
    res.send('Document deleted');
})

// Testing tambahkan user tanpa library si firebase function helper
app.get('/user2', (req, res) => {
    db.collection(user_)
        .get()
        .then(snapshot => {
            const data = {};
            data[user_] = {};
            snapshot.forEach(doc => {
                data[user_][doc.id] = doc.data();
            });
            res.status(200).send(data)
        });
})
